# RSA Implementation for Entropy Based Cost Function SCA

This repository contains the implementation of the Montgomery RSA exponentiation evaluated in the paper "Finding the Needle in the Haystack: Metrics for Best Trace Selection in Unsupervised Side-Channel Attacks on Blinded RSA", IEEE Transactions on Information Forensics and Security, 2021.

__DISCLAIMER__: This code is part of the above-mentioned publication and has been shown to be attackable on a STM32F303 target platform. Although, we made sure that all critical functions are executed in constant time on our platform, we especially do not claim that this implementation is secure.
**In particular, we do not provide any warranty regaring the security, i.e. do not use this code or parts of it in any product or real system.**

## Repository

The implementation of the exponentiation is contained in `src/rsa.c` and `mgomery.c`. The helper functions and the main executable demonstrate an RSA signing process and how functions are used.

## Requirements
The project was built and tested using `gmp` version 6.1.2 (newer version should work, too) and uses the meson build system. In most common distributions they are included in the repositories. 
E.g. for Ubuntu 20.04, the packages can be installed via
```bash
sudo apt install libgmp-dev meson
```
In order to test the implementation use the following commands:
```bash
# Generate the build folder with meson
meson build
# Compile the test program
ninja -C build

# The compiled program can be found under build/src/test-rsa
# Execute the test as
./build/src/test-rsa
```

## Notes for compiling on STM32F3xx microcontrollers
The following compiler flags were used to compile the firmware on the STM32F303 microcontroller:

``` -mcpu=cortex-m4 -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard```

Link with:

``` -lc -lm -lnosys -lgmp ```

The different countermeasures can be activated through setting the mode of the function ```rsa_sign_montgomery``` in ```src/rsa.c```. 
The mode ```RSA_MODE_RND_EXPONENT``` activates the exponent blinding and ```RSA_MODE_RND_MSG``` message randomization.
Modes can be combined using ```RSA_MODE_RND_EXPONENT | RSA_MODE_RND_MSG``` to enable exponent blinding and message randomization at the same time.

## License
The repository is licensed under the [Apache v2 License](LICENSE.txt).

## Contact
If you have any further questions feel free to [contact the authors](mailto:alexander.kulow@tum.de,t.schamberger@tum.de,lars.tebelmann@tum.de?%20&subject=TIFS%202021).
