/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#include "rsa.h"
#include "mgomery.h"

void rsa_sign_montgomery(mpz_t result, Msg *msg, Key *key, uint8_t mode){
	mpz_t r;
	mpz_t r2;
	mpz_t mod_p;
	mpz_t session_key;


	mpz_init2(r, KEY_LENGTH_BITS); 
	mpz_init2(r2, KEY_LENGTH_BITS); 
	mpz_init2(mod_p, KEY_LENGTH_BITS);
	mpz_init2(session_key, KEY_LENGTH_BITS + R1_BIT_SIZE);

	volatile int t = 0;
	mpz_t Rn[2];
	mpz_init2(Rn[0], KEY_LENGTH_BITS*2 + 1);
	mpz_init2(Rn[1], KEY_LENGTH_BITS*2 + 1);

	//Initialize montgomery parameters
	montgomery_prepare(r, r2, mod_p, KEY_LENGTH_BITS, key->N);


	if(mode & 1){ // exponent randomization
		mpz_mul(session_key, msg->r1, key->phiN);
		mpz_add(session_key, session_key, key->d);
	}
	else{
		mpz_set(session_key, key->d);
	}

	if(mode & 2){
		mpz_mul(Rn[1], msg->m, msg->r2);
		mpz_mod(Rn[1], Rn[1], key->N);
	}
	else{
		mpz_set(Rn[1], msg->m);
	}
	mpz_set(Rn[0], r);

	montgomery_mul(Rn[1], r2, r, mod_p, key->N);

	volatile int16_t bit_index = mpz_sizeinbase(session_key, 2) - 1;

	montgomery_mul(Rn[0], Rn[t], r, mod_p, key->N);
	t = 1;

	while(bit_index >= 0){
		montgomery_mul(Rn[0], Rn[t], r, mod_p, key->N);

		t ^= mpz_tstbit(session_key, bit_index);
		bit_index = bit_index - 1 + t;

	}

	mpz_swap(result, Rn[0]);
	

	mpz_set_ui(Rn[1], 1UL);
	montgomery_mul(result, Rn[1], r, mod_p, key->N);

	if(mode & 2){ 
		mpz_mul(result, result, msg->r2_inv);
		mpz_mod(result, result, key->N);
	}


	mpz_clear(Rn[0]);
	mpz_clear(Rn[1]);
	mpz_clear(r);
	mpz_clear(r2);
	mpz_clear(mod_p);
	mpz_clear(session_key);
}


void init_key(Key *key){
    mpz_init2(key->N, KEY_LENGTH_BITS + 8);
    mpn_zero(key->N->_mp_d + key->N->_mp_alloc - 1, 1); // clear chunk over modulus for addition w. carry
    mpz_init2(key->phiN, KEY_LENGTH_BITS);
    mpz_init2(key->d, KEY_LENGTH_BITS);
}
void init_msg(Msg *msg){
    mpz_init2(msg->m, KEY_LENGTH_BITS);
    mpz_init2(msg->r1, KEY_LENGTH_BITS);
    mpz_init2(msg->r2, KEY_LENGTH_BITS);
    mpz_init2(msg->r2_inv, KEY_LENGTH_BITS);
}

