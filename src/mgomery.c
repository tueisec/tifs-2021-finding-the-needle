/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/


#include "mgomery.h"

static mpz_t tmp;
static mpz_t tmp2;
static mpz_t test;

void mgom_init(){
	mpz_init2(test, KEY_LENGTH_BITS);
	mpz_init2(tmp, KEY_LENGTH_BITS* 3);
	mpz_init2(tmp2, KEY_LENGTH_BITS * 2 + 1);
	
}

#ifdef DEMO
//if executed on recent x86 architectures, the limb size is 64. Therefore, the schoolbook notation as in the DUT
//implementation does not work since it is only for 32 bit.
//in the DUT case, the implementation in the else branch shall be used.
//mpn_sec_mul in the DEMO section is used only to verify that the algorithm calculates correctly.
//the mpn_sec_mul function is NOT constant time on our device which is problematic for side-channel analysis.

void const_mul(mp_limb_t *rp, const mp_limb_t *s1p, mp_size_t s1n, const mp_limb_t *s2p, mp_size_t s2n){
	mp_size_t scratch_n = mpn_sec_mul_itch (s1n, s2n);
	mpz_t scratch;
	mpz_init2(scratch, sizeof(unsigned long) * 8 * scratch_n);
	mpn_sec_mul(rp, s1p, s1n, s2p, s2n, scratch->_mp_d);
	mpz_clear(scratch);

}
#else
// Follow schoolbook notation by Bouman https://arxiv.org/pdf/1804.07236.pdf
void const_mul(mp_limb_t *rp, const mp_limb_t *s1p, mp_size_t s1n, const mp_limb_t *s2p, mp_size_t s2n){
	for(uint8_t u = 0; u < s1n + s2n; u++){
		rp[u] = 0;
	}
	for(uint8_t al=0; al < s2n;al++){
		uint32_t w = 0;
		uint8_t bl = 0;
		for(bl = 0; bl < s1n; bl++){
			uint64_t s = (uint64_t) s2p[al] * s1p[bl] + rp[bl + al] + w;
			rp[bl + al] = s & 0xFFFFFFFFUL;
			w = s >> 32;
		}
		rp[bl + al] += w;
	}
}
#endif



//cf. Menezes et al. Handbook of Applied cryptography Alg 14.36
void montgomery_prepare(mpz_t r, mpz_t r2, mpz_t mod_p, const int bit_length, const mpz_t modulus){
	mpz_set_ui(r, 0UL);
	mpz_setbit(r, KEY_LENGTH_BITS);

	mpz_powm_ui(r2, r, 2, modulus);
	mpz_neg(test, modulus);

	mpz_invert(mod_p, test, r);
}

void montgomery_mul(mpz_t a, const mpz_t b, const mpz_t r, const mpz_t mod_p, mpz_t modulus){

	const_mul(tmp->_mp_d, a->_mp_d, a->_mp_size, b->_mp_d, b->_mp_size);

	//since we are using low-level functions of GMP, we need to calculate the sizes ourselves
	tmp->_mp_size = a->_mp_size + b->_mp_size;

	mpz_set(a, tmp);
	const_mul(tmp->_mp_d, a->_mp_d, a->_mp_size, mod_p->_mp_d, mod_p->_mp_size);
	
	tmp->_mp_size = a->_mp_size + mod_p->_mp_size;


	for(int i = 0; i < (KEY_LENGTH_BITS / LIMB_SIZE); i++){
		tmp2->_mp_d[i] = tmp->_mp_d[i];
	}
	tmp2->_mp_size = KEY_LENGTH_BITS / LIMB_SIZE;

	const_mul(tmp->_mp_d, modulus->_mp_d, modulus->_mp_size, tmp2->_mp_d, tmp2->_mp_size);
	
	tmp->_mp_size = modulus->_mp_size + tmp2->_mp_size;
	mpn_zero(tmp->_mp_d + tmp->_mp_size, tmp->_mp_alloc - tmp->_mp_size);
	mpn_zero(a->_mp_d + a->_mp_size, a->_mp_alloc - a->_mp_size);


	unsigned s = 2* modulus->_mp_size + 1;
	mpn_add_n(tmp->_mp_d, a->_mp_d, tmp->_mp_d, s);
	tmp->_mp_size = s;

	tmp->_mp_d += KEY_LENGTH_BITS/LIMB_SIZE; // perform rshift
	tmp->_mp_size = tmp->_mp_size - KEY_LENGTH_BITS / LIMB_SIZE;

	modulus->_mp_size += 1;

	#ifdef DEMO
	uint64_t cmp[2] = {0,0};
	uint64_t t = 0;
	#else
	uint32_t cmp[2] = {0,0};
	uint32_t t = 0;
	#endif

	// this effectively compares tmp > mod in const time.
	// if mod is greater, all comparisons are directed into cmp[1], cmp[0] remains 0
	for(int i = tmp->_mp_size - 1; i >= 0; i--){
		cmp[t] = tmp->_mp_d[i] > modulus->_mp_d[i];
		t |= !(tmp->_mp_d[i] == modulus->_mp_d[i]);
	}

	volatile unsigned long ovf = cmp[0];
	modulus->_mp_size -= 1;

	mpn_cnd_sub_n(ovf, a->_mp_d, tmp->_mp_d, modulus->_mp_d, tmp->_mp_size);
	a->_mp_size = modulus->_mp_size;

	tmp->_mp_d -= KEY_LENGTH_BITS/LIMB_SIZE; // revert pointer

}
