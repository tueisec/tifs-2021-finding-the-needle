/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#include <stdio.h>
#include "deps.h"
#include "rsa.h"
#include "helper.h"
#include "mgomery.h"

int main(int argc, char const *argv[])
{
	Key key;
	Msg msg;
	mpz_t result;

	//for demonstration purposes, we can use rand() here.
	srand(time(NULL));

	mpz_init2(result, KEY_LENGTH_BITS * 2 + 8); //8 bits for reserve
	init_key(&key);
	init_msg(&msg);
	mgom_init();
	helper_gen_privkey(&key);
	helper_gen_msg(&msg, &key);

	rsa_sign_montgomery(result, &msg, &key, RSA_MODE_RND_EXPONENT | RSA_MODE_RND_MSG);

	#ifdef DEMO
	gmp_printf("result is: %Zx", result);
	#endif
	return 0;
}
