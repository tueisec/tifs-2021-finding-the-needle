/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#include "rsa.h"

/*
generic helper functions to generate valid key and message structs. For performance reasons,
do not generate the key on the DUT for SCA, but pass it via e.g. UART to it. Typically you want a Python
script on the x86 machine generating the primes and calculate the key, and then pass the data to your DUT and
fill the structs.

*/

void helper_gen_privkey(Key *key){
	mpz_t p;
	mpz_t p1;
	mpz_t q;
	mpz_t q1;
	mpz_t prime_size;
	mpz_t pub_exp;

	mpz_init(p);
	mpz_init(p1);
	mpz_init(q);
	mpz_init(q1);
	mpz_init(prime_size);
	mpz_init(pub_exp);

	mpz_set_ui(pub_exp, 65537);

	//generate a prime that is at least KEY_LENGTH_BITS/2, with some reserve
	mpz_ui_pow_ui(prime_size, 2, ((KEY_LENGTH_BITS/2)-2));

	//do not use the standard rand() function for production purposes, demo only
	mpz_add_ui(prime_size, prime_size, rand());
	mpz_nextprime(p, prime_size);
	mpz_add_ui(prime_size, prime_size, rand());
	mpz_nextprime(q, prime_size);

	mpz_mul(key->N, p, q);
	mpz_sub_ui(p1, p, 1);
	mpz_sub_ui(q1, q, 1);
	mpz_mul(key->phiN, p1, q1);

	mpz_invert(key->d, pub_exp, key->phiN);
	#ifdef DEMO
	gmp_printf ("p is %Zx\n", p);
	gmp_printf ("q is %Zx\n", q);
	gmp_printf ("N is %Zx\n", key->N);
	gmp_printf ("phiN is %Zx\n", key->phiN);
	gmp_printf ("d is %Zx\n", key->d);
	#endif

	mpz_clear(p);
	mpz_clear(q);
	mpz_clear(prime_size);
	mpz_clear(pub_exp);
}

void helper_gen_msg(Msg *msg, Key* key){
	gmp_randstate_t state;
	gmp_randinit_default (state);

	
	gmp_randseed_ui(state, rand());
	mpz_urandomb(msg->r1, state, R1_BIT_SIZE);
	mpz_urandomb(msg->r2, state, R2_BIT_SIZE);
	mpz_urandomb(msg->m, state, KEY_LENGTH_BITS-8);

	mpz_t inverse_base;
	mpz_init(inverse_base);
	mpz_powm(inverse_base, msg->r2, key->d, key->N);
	mpz_invert(msg->r2_inv, inverse_base, key->N);

	#ifdef DEMO
	gmp_printf ("r1 is %Zx\n", msg->r1);
	gmp_printf ("r2 is %Zx\n", msg->r2);
	gmp_printf ("m is %Zx\n", msg->m);
	gmp_printf ("r2_inv is %Zx\n", msg->r2_inv);
	#endif


	mpz_clear(inverse_base);

}