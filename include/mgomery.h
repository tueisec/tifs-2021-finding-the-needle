/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

#ifndef MGOMERY_H
#define MGOMERY_H

#include "rsa.h"
void montgomery_prepare(mpz_t r, mpz_t r2, mpz_t mod_p, const int bit_length, const mpz_t modulus);
void montgomery_mul(mpz_t a, const mpz_t b, const mpz_t r, const mpz_t mod_p, mpz_t modulus);
void mgom_init();
#endif
