/*

Copyright 2021 Technical University of Munich

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
#ifndef RSA_NAIVE_H
#define RSA_NAIVE_H

#include "deps.h"

extern volatile uint32_t inst_cnt[200];
extern volatile uint8_t cntx;

typedef struct s_key{
    mpz_t N;
    mpz_t phiN;
    mpz_t d;
} Key;

typedef struct s_msg{
    mpz_t m;
    mpz_t r1;
    mpz_t r2;
    mpz_t r2_inv;
} Msg;

#define KEY_LENGTH_BITS 1024

#define R1_BIT_SIZE	64
#define R2_BIT_SIZE	64
#define LIMB_SIZE (sizeof(unsigned long)*8) //verify that this is 32 on your DUT. On a STM32F303, this is the case.


#define RSA_MODE_RND_EXPONENT 1
#define RSA_MODE_RND_MSG 2

void rsa_sign_montgomery(mpz_t result, Msg *msg, Key *key, uint8_t mode);

void init_key(Key *key);
void init_msg(Msg *msg);

#endif
